package herencia;

public class Estudiante {
    protected String nombre;
    protected String carrera;
    protected int edad;

    public String mostrarInfo(){
        return  "El estudiante " +nombre+" de "+edad+"años, cursa "+carrera+".";
    }
}
