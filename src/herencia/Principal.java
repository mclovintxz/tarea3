package herencia;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;

public class Principal {
    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    public static void main (String[] args){

        EstudSistemas informatico = new EstudSistemas();
        EstudAbogacia abogado = new EstudAbogacia();

        informatico.nombre = "Josue";
        informatico.edad = 22;
        informatico.carrera = "Sistemas";

        abogado.nombre = "Erick";
        abogado.edad = 32;
        abogado.carrera = "Abogacia";

        out.println(informatico.mostrarInfo());
        out.println(abogado.mostrarInfo());

        out.println("Cantidad de materias Informatico: " +informatico.getCantMaterias());
        out.println("Cantidad de materias Abogado: " +abogado.getCantMaterias());

    }
}
